package com.etherealvendetta.bloodbank;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Created by Sagar on 1/14/2017.
 */

public class BBAdapter extends RecyclerView.Adapter<BBAdapter.MyViewHolder> {

    private Context contex;
    private LayoutInflater inflater;
    List<BloodBank> data = Collections.emptyList();

    public BBAdapter(Context context,List<BloodBank> data)
    {
        this.contex = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        Log.d("adapter initialized","inside");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = inflater.inflate(R.layout.bb_row,parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        Log.d("onCreate View Holder","inside");
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position)
    {

        BloodBank current = data.get(position);
        holder.title.setText(current.title);
        holder.address.setText(current.address);
        holder.icon.setImageResource(R.drawable.blood_drop_icon);// Can Be Changed BB Image

        Log.d("onBind View Holder","inside");


    }

    @Override
    public int getItemCount() {
        Log.d("getitem", "getItemCount: " +data.size());
        return data.size();

    }
    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView address;
        ImageView icon;

        public MyViewHolder(View itemView) {


            super(itemView);
            //itemView.setOnClickListener(this);
            title = (TextView) itemView.findViewById(R.id.bbTitle);
            address = (TextView) itemView.findViewById(R.id.bbAddress);
            icon = (ImageView) itemView.findViewById(R.id.bbIcon);
            address.setOnClickListener( new View.OnClickListener() {

                @Override
                public void onClick(View view)
                {
                Homescreen hm = (Homescreen) contex;
                try {
                    hm.goToHospital((String) title.getText());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }});
            title.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Homescreen hm = (Homescreen) contex;
                        hm.GoToInventory();
                    }
                });
            icon.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Homescreen hm = (Homescreen) contex;
                    hm.CurrentInventory();
                }
            });
            Log.d("my View Holder","inside");
        }





    }
}
