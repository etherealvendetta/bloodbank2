package com.etherealvendetta.bloodbank;

import android.*;
import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import android.os.Debug;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Homescreen extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private BBAdapter adapter;
    private RecyclerView mRecyclerView;
    static final Integer FINE_LOCATION= 0x1;
    static final Integer COARSE_LOCATION = 0x2;
    GoogleMap mGoogleMap;
    GoogleApiClient mGoogleApiClient;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (IsPlayServicesAvailable()) {
            Toast.makeText(this, "Perfect", Toast.LENGTH_LONG).show();
            setContentView(R.layout.activity_homescreen);
            initMap();


            mRecyclerView = (RecyclerView) findViewById(R.id.bloodbankList);
            adapter = new BBAdapter(this,GetData());
            mRecyclerView.setAdapter(adapter);
            Log.d(adapter.toString(),mRecyclerView.toString());
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            //GetBloodBanks(this.getCurrentFocus());
        }
        //No GOogle Play Service
        else {

        }


    }
    //Prepare the list of hospital data
    //Currently hardcoded , TODO : parse from XML/Json
    //Will need to create a seperate function for Filtering of Blood Banks
    public List<BloodBank> GetData() {
        Log.d("GetData","inside Getdata");
        List<BloodBank> data = new ArrayList<>();
        int[] icons = {R.drawable.blood_drop_icon,R.drawable.blood_drop_icon,R.drawable.blood_drop_icon,R.drawable.blood_drop_icon};
        String[] titles = {"Saifee Hospital","Siddharth Hospital","KB Bhabha Hospital, Mumbai, Maharashtra, India","HBT Medical College"};
        String[] addresses = {"15/17, Maharishi Karve Marg, Opposite to Charni Road Station Mumbai"," Siddharth Nagar, Prabhodhan Kridabhawan Marg, Goregaon (W)"," R. K. Patkar Marg, Bandra (W), Mumbai"," North south Road No-1, Juhu, Vile-Parle (W) Mumbai"};
        for(int i = 0; i <4; i++)
        {
            BloodBank current = new BloodBank();
            current.title= titles[i];
            current.address= addresses[i];
            current.hosId= icons[i];
            data.add(current);
            Log.d("dataset","inside");
           // geoLocate(titles[i]);
        }

        Log.d("data ", "GetData: " +data);

        return data;
    }


    private void initMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);

    }


    public boolean IsPlayServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {

            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(this, "Cant Connect to Play Services", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        //goToLocationZoom(19.2422273,72.9713287,15);

       if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
           // Should we show an explanation?
           //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
           ask();
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();
    }
    //Removed Funtionality
    // Kept Just in case
    private void goToLocation(double lat, double lng) {
        LatLng ll = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLng(ll);
        mGoogleMap.moveCamera(update);
    }
    //Removed Funtionality
    // Kept Just in case
    private void goToLocationZoom(double lat, double lng, float zoom) {
        LatLng ll = new LatLng(lat, lng);

        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        mGoogleMap.moveCamera(update);


    }
    //focus on the BloodBank location
    public void goToHospital(String hos) throws IOException {
        Geocoder gc = new Geocoder(this);
        List<Address> list = gc.getFromLocationName(hos,2);
        Address address = list.get(0);
        String hosname = address.getFeatureName();
        double lat = address.getLatitude();
        double lng = address.getLongitude();
        LatLng ll = new LatLng(lat,lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll,14);
        mGoogleMap.animateCamera(update);

        addBloodMarker(hosname, lat, lng);

    }
    //locating  and marking the cards in the map
    public void geoLocate(String loc) throws IOException {
        Geocoder gc = new Geocoder(this);
        List<Address> list = gc.getFromLocationName(loc, 2);
        Address address = list.get(0);
        Log.d("add 1", "geoLocate: " + list.get(0));
//        Log.d("add 2", "geoLocate: " + list.get(1));
        String hosname = address.getFeatureName();
        double lat = address.getLatitude();
        double lng = address.getLongitude();
        addBloodMarker(hosname, lat, lng);
    }

    private void addBloodMarker(String hosname, double lat, double lng) {
        MarkerOptions options = new MarkerOptions()
                .title(hosname)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.blood_drop_icon))
                .position(new LatLng(lat,lng));
        mGoogleMap.addMarker(options);
    }
    Marker userMarker = null;
    private void addCurrentMarker( double lat, double lng) {

        if(userMarker != null)
            userMarker = null;
        MarkerOptions options = new MarkerOptions()
                .title("You are Here")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                .position(new LatLng(lat,lng));
         userMarker =  mGoogleMap.addMarker(options);
    }

    LocationRequest mLocationRequest;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling//
            // @sagar:Handled
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private boolean initialized = false;//tried to make it so that user location gets in focus . Doesnt work as intended xD
    @Override
    public void onLocationChanged(Location location) {

        if(location==null)
        {
            Toast.makeText(this, "Cant Get Current Location", Toast.LENGTH_SHORT).show();
        }else
        if(initialized = false)
        {
            LatLng ll = new LatLng(location.getLatitude(),location.getLongitude());
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll,11);
            mGoogleMap.animateCamera(update);
            addCurrentMarker(ll.latitude,ll.longitude);
            initialized = true;
        }
    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(Homescreen.this, permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(Homescreen.this, permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(Homescreen.this, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(Homescreen.this, new String[]{permission}, requestCode);
            }
        } else {
            Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }
    public void ask()
    {
          askForPermission(Manifest.permission.ACCESS_FINE_LOCATION,FINE_LOCATION);
          askForPermission(Manifest.permission.ACCESS_COARSE_LOCATION,COARSE_LOCATION);
    }


    public void GetBloodBanks(View view) throws IOException {

        String[] titles = {"Saifee Hospital","Siddharth Hospital","KB Bhabha Hospital, Mumbai, Maharashtra, India","HBT Medical College"};
        for(int i = 0;i<4;i++) {
            geoLocate(titles[i]);
        }
    }

    public void GoToInventory()
    {
        Intent intent = new Intent(this,Inventory.class);
        startActivity(intent);

    }

    public void CurrentInventory()
    {
        Intent intent = new Intent(this,ViewInventory.class);
        startActivity(intent);

    }
}
